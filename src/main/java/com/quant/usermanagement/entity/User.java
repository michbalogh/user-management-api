package com.quant.usermanagement.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Date;

@RedisHash("user")
public class User {

    @Getter
    @Setter
    @Id
    @NotNull
    private String username;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String surname;

    @Getter
    @Setter
    @Email
    private String email;

    @Getter
    @Setter
    private String role;

    @Getter
    @Setter
    private Date registrationDate;

    @Getter
    @Setter
    private boolean enabled;

}
