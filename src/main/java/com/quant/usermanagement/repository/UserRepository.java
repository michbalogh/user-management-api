package com.quant.usermanagement.repository;

import com.quant.usermanagement.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
}
