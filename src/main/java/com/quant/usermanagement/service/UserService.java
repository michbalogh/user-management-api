package com.quant.usermanagement.service;

import com.quant.usermanagement.to.UserTo;

import java.util.List;

public interface UserService {

    List<UserTo> findAll();

    UserTo findById(String id);

    UserTo create(UserTo user);

    void delete(String id);

    UserTo update(String id, UserTo user);
}
