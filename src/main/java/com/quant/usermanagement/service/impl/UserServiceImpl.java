package com.quant.usermanagement.service.impl;

import com.quant.usermanagement.entity.User;
import com.quant.usermanagement.exception.DuplicatedEntryException;
import com.quant.usermanagement.exception.EmailUniqueException;
import com.quant.usermanagement.exception.ResourceNotFoundException;
import com.quant.usermanagement.repository.UserRepository;
import com.quant.usermanagement.service.UserService;
import com.quant.usermanagement.to.UserTo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private ModelMapper mapper = new ModelMapper();

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserTo> findAll() {
        List<User> users = (List<User>) userRepository.findAll();
        return users.stream().map(user -> mapper.map(user, UserTo.class)).collect(Collectors.toList());
    }

    @Override
    public UserTo findById(String id) {
        return userRepository.findById(id).map(user -> mapper.map(user, UserTo.class))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("User %s not found", id)));
    }

    @Override
    public UserTo create(UserTo user) {
        userRepository.findById(user.getUsername()).ifPresent(foundUser -> {
            throw new DuplicatedEntryException(String.format("User with username %s already exists.", foundUser.getUsername()));
        });
        userRepository.findAll().forEach(userFromDb -> {
            if (userFromDb.getEmail().equals(user.getEmail())) {
                throw new DuplicatedEntryException(String.format("User with email %s already exists.", user.getUsername()));
            }
        });
        User userEntity = mapper.map(user, User.class);
        return mapper.map(userRepository.save(userEntity), UserTo.class);
    }

    @Override
    public void delete(String id) {
        User userEntity = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("User %s not found", id)));
        userRepository.delete(userEntity);
    }

    @Override
    public UserTo update(String id, UserTo user) {
        userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("User %s not found", id)));
        userRepository.findAll().forEach(userFromDb -> {
            if (userFromDb.getEmail().equals(user.getEmail()) && !userFromDb.getUsername().equals(user.getUsername())) {
                throw new EmailUniqueException(String.format("User with email %s already exists.", user.getEmail()));
            }
        });
        User userEntity = mapper.map(user, User.class);
        return mapper.map(userRepository.save(userEntity), UserTo.class);
    }
}
