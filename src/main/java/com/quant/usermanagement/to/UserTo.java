package com.quant.usermanagement.to;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

public class UserTo implements Serializable {

    private static final long serialVersionUID = 5327766680748073213L;

    @Getter
    @Setter
    @Id
    @NotNull
    private String username;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String surname;

    @Getter
    @Setter
    @Email
    private String email;

    @Getter
    @Setter
    private String role;

    @Getter
    @Setter
    private Date registrationDate;

    @Getter
    @Setter
    private boolean enabled;

}
